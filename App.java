package com.mycompany.calculator;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
        
        // instantiating buttons
        

        Button equals = new Button("="),zero = new Button("0"), one = new Button("1"), two = new Button("2"), 
        three = new Button("2"), four = new Button("4"), five = new Button("5"), six = new Button("6"), 
        seven = new Button("7"), eight = new Button("8"), nine = new Button("9"), add = new Button("+"), 
        subtract = new Button("-"), multiply = new Button("X"), divide = new Button("/"), clear = new Button("C");
        


        
        TextField input = new TextField();

        
        GridPane r = new GridPane(); 
        GridPane f = new GridPane();
        //r.getChildren().addAll(equals,zero,one,two,three,four,five,six,seven,eight,nine,subtract,multiply,divide);
        
        
        var scene = new Scene(r, 300, 500);
        

        r.add(input, 0,0,4,1);  
        r.addRow(1, seven, eight, nine, add );  
        r.addRow(2, four, five, six, subtract );
        r.addRow(3, one, two, three, multiply );
        r.addRow(4, zero, equals, divide, clear);


         

        stage.setTitle("Calculator");
        stage.setScene(scene);
        
        
        one.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + "1"); 
        });
        two.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + "2"); 

        });
        three.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + "3"); 

        });
        four.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + "4");
        });
        five.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + "5");
        });
        six.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + "6");
        });
        seven.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + "7");
        });
        eight.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + "8");
        });
        nine.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + "9");
        });
        add.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + " + ");
        });
        subtract.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + " - ");
        });
        divide.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + " / ");
        });
        multiply.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            input.setText(userInput + " * ");
        });
        clear.setOnAction(e -> {
            input.clear();
        });
        
        equals.setOnAction(e -> {
            String userInput = String.valueOf(input.getText());
            
            
            String[] splitInput = userInput.split(" ");
            int inputOne = Integer.parseInt(splitInput[0]);
            int inputTwo = Integer.parseInt(splitInput[2]);
            
            
            if (splitInput[1].equals("+") == true){
                input.setText(String.valueOf(inputOne+inputTwo));        
            }
            else if(splitInput[1].equals("-") == true){
                input.setText(String.valueOf(inputOne-inputTwo)); 
            }
            else if (splitInput[1].equals("/") == true){
                input.setText(String.valueOf(inputOne/inputTwo)); 
            }
            else if (splitInput[1].equals("*")== true){
                input.setText(String.valueOf(inputOne*inputTwo)); 
            }
            
        });
        
        stage.show();
    }
    

    public static void main(String[] args) {
        launch();
    }

}